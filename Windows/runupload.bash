#!/bin/bash
SSHHOST=exodus_nightly@brachium.dreamhost.com
SSHPATH=/home/exodus_nightly/nightly.exodusviewer.com
cd $1
echo Setting file perms...
chmod -R u+r .
chmod -R g-r .
chmod -R o-r .
echo Beginning upload...
ssh $SSHHOST -C "mkdir -p $SSHPATH/$2" && rsync --progress -avz . $SSHHOST:/$SSHPATH/$2/ && ssh $SSHHOST -C "cd $SSHPATH/$2 && chmod -vR a+r ."
echo Upload script ended.