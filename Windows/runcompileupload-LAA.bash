#!/bin/bash
TEMP_LOC=/cygdrive/z/temp/

if [ "$1" == "" ]
then
	REV="0"
else
	REV="$1"
fi

YY="$[ $(date +%-y) ]"
MM="$[ $(date +%-m) ]"
DD="$[ $(date +%-d) ]"
OUTPUTFILE="ExodusViewer-$YY.$MM.$DD.$REV.exe"

echo "Generating llversionviewer.h on the fly and changing login screen for: $(printf "%02d" $YY) $(printf "%02d" $MM) $(printf "%02d" $DD) $(printf "%02d" $REV) DEVELOPMENT"
echo "Exodus Viewer Development Build $(printf "%02d" $YY) $(printf "%02d" $MM) $(printf "%02d" $DD) $(printf "%02d" $REV)" > build.txt
echo "#ifndef LL_LLVERSIONVIEWER_H
#define LL_LLVERSIONVIEWER_H

const S32 LL_VERSION_MAJOR = $YY;
const S32 LL_VERSION_MINOR = $MM;
const S32 LL_VERSION_PATCH = $DD;
const S32 LL_VERSION_BUILD = $REV;

const char * const EXO_VERSION_STRING = \"$(printf "%02d" $YY).$(printf "%02d" $MM).$(printf "%02d" $DD).$(printf "%02d" $REV)\";
const char * const EXO_VERSION_STRING_SHORT = \"$(printf "%02d" $YY).$(printf "%02d" $MM).$(printf "%02d" $DD)\";

#ifdef LL_WINDOWS
const char * const EXO_AGENT_VERSION = \"Win/$(printf "%02d" $YY).$(printf "%02d" $MM).$(printf "%02d" $DD).$(printf "%02d" $REV)/DEVELOPMENT\";
#else
#ifdef LL_DARWIN
const char * const EXO_AGENT_VERSION = \"Mac/$(printf "%02d" $YY).$(printf "%02d" $MM).$(printf "%02d" $DD).$(printf "%02d" $REV)/DEVELOPMENT\";
#else
const char * const EXO_AGENT_VERSION = \"Lin/$(printf "%02d" $YY).$(printf "%02d" $MM).$(printf "%02d" $DD).$(printf "%02d" $REV)/DEVELOPMENT\";
#endif
#endif

const char * const LL_CHANNEL = \"Exodus Viewer DEVELOPMENT\";

#if LL_DARWIN
const char * const LL_VERSION_BUNDLE_ID = \"com.secondlife.exodus.viewer\";
#endif

#endif // LL_LLVERSIONVIEWER_H
" > indra/llcommon/llversionviewer.h
sed -i 's/Exodus Viewer Beta/Exodus Viewer DEVELOPMENT/g' ./indra/cmake/Variables.cmake
sed -i 's/app\.exodusviewer\.com\/login/app\.exodusviewer\.com\/devlogin/g' ./indra/newview/llviewernetwork.cpp
rm -f build-vc100/newview/Release/$OUTPUTFILE

echo Running build process.
runbuild.bat LAA &>> build.txt

if [ -f "./build-vc100/newview/Release/$OUTPUTFILE" ]
then
	echo Build success.
	rm -rf $TEMP_LOC*
	cp -f build-vc100/newview/Release/$OUTPUTFILE $TEMP_LOC
	cp -f build.txt $TEMP_LOC
	echo "Running command: runupload.bash $TEMP_LOC /Windows-x86/$(printf "%02d" $YY)/$(printf "%02d" $MM)/$(printf "%02d" $DD)/$(printf "%02d" $REV)"
	runupload.bash $TEMP_LOC /Windows-x86-LAA-64Bit/$(printf "%02d" $YY)/$(printf "%02d" $MM)/$(printf "%02d" $DD)/$(printf "%02d" $REV)
else
	echo Build failure.
fi
